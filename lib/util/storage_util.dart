/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021



import 'package:shared_preferences/shared_preferences.dart';
import 'logger.dart';



class  StorageUtil{

  static const String CLASS_NAME = 'StorageUtil';


  static Future<void> setDataInSP(String key, String data) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, data);
  }

  static Future<String?> getDataFromSP(String key) async{
    try {
      final SharedPreferences? prefs = await SharedPreferences.getInstance();
      final String? result = prefs!.getString(key);
      return result;
    }catch(e){
      Logger.log(CLASS_NAME, 'getDataFromSP', e.toString());
      return null;
    }
  }

  static Future<void> setDataInSPInt(String key, int data) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, data);
  }

  static Future<int?> getDataFromSPInt(String key) async{
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final int? result = prefs.getInt(key);
      return result;
    }catch(e){
      Logger.log(CLASS_NAME, 'getDataFromSPInt', e.toString());
      return null;
    }
  }

  static Future clearAllSP() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

}
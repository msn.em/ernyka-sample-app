/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021

import 'package:untitled/common/configs.dart';

class Logger {

  static void log(String className, String methodName, String errorText) {
    if (Configs.IS_DEBUG_MODE)
      print('-----------------------------------------------------------------\n'
          'class: $className, method: $methodName, error: $errorText'
          '\n----------------------------------------------------------------');
  }
}

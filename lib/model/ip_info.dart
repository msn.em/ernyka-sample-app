/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'package:flutter/material.dart';



class IpInfo {
  String _ip = "";
  String _country = "";
  String _cc = "";

  IpInfo({required String ip, required String country, required String cc}) {
    this._ip = ip;
    this._country = country;
    this._cc = cc;
  }

  String get ip => _ip;
  set ip(String ip) => _ip = ip;
  String get country => _country;
  set country(String country) => _country = country;
  String get cc => _cc;
  set cc(String cc) => _cc = cc;

  IpInfo.fromJson(Map<String, dynamic> json) {
    _ip = json['ip'];
    _country = json['country'];
    _cc = json['cc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ip'] = this._ip;
    data['country'] = this._country;
    data['cc'] = this._cc;
    return data;
  }
}

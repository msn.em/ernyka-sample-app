/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'package:flutter/material.dart';

import 'home.dart';


class Profile extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    double _screenWidth = MediaQuery.of(context).size.width;
    double _screenHeight = MediaQuery.of(context).size.height;

    return Container(alignment: Alignment.center, child: Icon(Icons.supervised_user_circle, size: 150));
  }
}

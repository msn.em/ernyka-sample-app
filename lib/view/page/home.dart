/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:untitled/service/app_localizations.dart';
import 'package:untitled/state/home_provider.dart';
import 'package:untitled/view/page/default.dart';
import 'package:untitled/view/page/profile.dart';
import 'package:untitled/view/page/settings.dart';

import 'messages.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  HomeProvider _homeProvider = new HomeProvider();

  _handleBottomNavBarOnTap(int index) {
    switch (index) {
      case 0:
        {
          _homeProvider.setNavBarIndex(0);
          _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('home'));
          break;
        }
      case 1:
        {
          _homeProvider.setNavBarIndex(1);
          _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('settings'));
          break;
        }
      case 2:
        {
          _homeProvider.setNavBarIndex(2);
          _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('profile'));
          break;
        }
      case 3:
        {
          _homeProvider.setNavBarIndex(3);
          _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('messages'));
          break;
        }
    }
  }

  Widget? _getPageBySelectedIndex(int index) {
    switch (index) {
      case 0:
        {
          return Default();
        }
      case 1:
        {
          return Settings();
        }
      case 2:
        {
          return Profile();
        }
      case 3:
        {
          return Messages();
        }
    }
  }

  Future<void> _showExitDialog(BuildContext con) async {
    Widget okButton = TextButton(
      child: Text(AppLocalizations.of(context)!.translate('yes')),
      onPressed: () {
        Navigator.of(con).pop();
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      },
    );

    Widget cancelButton = TextButton(
        child: Text(AppLocalizations.of(context)!.translate('no')),
        onPressed: () {
          Navigator.of(con).pop();
        });

    AlertDialog alert = AlertDialog(
        title: Text(AppLocalizations.of(context)!.translate('exit')),
        content: Text(AppLocalizations.of(context)!.translate('msg_confirm_exit')),
        actions: [okButton, cancelButton]);

    await showDialog(
        context: con,
        builder: (BuildContext ctx) {
          return alert;
        });
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _homeProvider = Provider.of<HomeProvider>(context, listen: false);
    });

    super.initState();
  }

  List<BottomNavigationBarItem> _getBottomNavBarItems(BuildContext c) {
    var _bottomNavBarItems = [
      BottomNavigationBarItem(
          icon: Icon(Icons.home, color: Colors.grey),
          activeIcon: Icon(Icons.home, color: Colors.white),
          label: AppLocalizations.of(c)!.translate('home')),
      BottomNavigationBarItem(
          icon: Icon(Icons.settings, color: Colors.grey),
          activeIcon: Icon(Icons.settings, color: Colors.white),
          label: AppLocalizations.of(c)!.translate('settings')),
      BottomNavigationBarItem(
          icon: Icon(Icons.account_box, color: Colors.grey),
          activeIcon: Icon(Icons.account_box, color: Colors.white),
          label: AppLocalizations.of(c)!.translate('profile')),
      BottomNavigationBarItem(
          icon: Icon(Icons.message, color: Colors.grey),
          activeIcon: Icon(Icons.message, color: Colors.white),
          label: AppLocalizations.of(c)!.translate('messages'))
    ];
    return _bottomNavBarItems;
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    return Consumer<HomeProvider>(builder: (_, __, ___) {
      return Directionality(
          textDirection: AppLocalizations.of(context)!.locale.languageCode == 'fa' ? TextDirection.rtl : TextDirection.ltr,
          child: SafeArea(
              child: Theme(
                  data: ThemeData(
                      brightness: _homeProvider.isThemeDark ? Brightness.dark : Brightness.light, fontFamily: 'iransans'),
                  child: Scaffold(
                      appBar: AppBar(title: Text(_homeProvider.currentPageTitle), centerTitle: true),
                      body: _getPageBySelectedIndex(_homeProvider.currentNavBarIndex),
                      bottomNavigationBar: Theme(
                          data: Theme.of(context)
                              .copyWith(canvasColor: _homeProvider.isThemeDark ? Colors.black38 : Colors.blue[800]),
                          child: BottomNavigationBar(
                              items: _getBottomNavBarItems(context),
                              currentIndex: _homeProvider.currentNavBarIndex,
                              elevation: 10,
                              onTap: (int index) {
                                _handleBottomNavBarOnTap(index);
                              })),
                      drawer: Drawer(
                          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                        Padding(
                            padding: EdgeInsets.only(top: 20, bottom: 20),
                            child: Image.asset('asset/image/avatar.png', width: screenWidth * 0.35)),
                        Padding(padding: EdgeInsets.all(8), child: Divider(color: Colors.grey[700], thickness: 1.3)),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: GestureDetector(
                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                Text(AppLocalizations.of(context)!.translate('messages')),
                                Expanded(child: Container()),
                                Icon(Icons.message)
                              ]),
                              onTap: () {
                                Navigator.pop(context);
                                _homeProvider.setNavBarIndex(3);
                                _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('messages'));
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(8), child: Divider(color: Colors.grey[700])),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: GestureDetector(
                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                Text(AppLocalizations.of(context)!.translate('settings')),
                                Expanded(child: Container()),
                                Icon(Icons.settings)
                              ]),
                              onTap: () {
                                Navigator.pop(context);
                                _homeProvider.setNavBarIndex(1);
                                _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('settings'));
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(8), child: Divider(color: Colors.grey[700])),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: GestureDetector(
                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                Text(AppLocalizations.of(context)!.translate('profile')),
                                Expanded(child: Container()),
                                Icon(Icons.account_box)
                              ]),
                              onTap: () {
                                Navigator.pop(context);
                                _homeProvider.setNavBarIndex(2);
                                _homeProvider.setPageTitle(AppLocalizations.of(context)!.translate('profile'));
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(8), child: Divider(color: Colors.grey[700])),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: GestureDetector(
                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                Text(AppLocalizations.of(context)!.translate('exit')),
                                Expanded(child: Container()),
                                Icon(Icons.exit_to_app)
                              ]),
                              onTap: () {
                                Navigator.pop(context);
                                _showExitDialog(context);
                              }),
                        )
                      ]))))));
    });
  }
}

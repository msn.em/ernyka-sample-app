/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'dart:async';
import 'package:flutter/material.dart';
import 'home.dart';


class Splash extends StatefulWidget {

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  late Timer timer;

  @override
  void initState() {
    super.initState();
    var duration = const Duration(seconds: 4);
    timer = Timer(duration, (){
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (BuildContext con) => Home())).then((_) => timer.cancel());
    });
  }

  @override
  Widget build(BuildContext context) {

    double _screenWidth = MediaQuery.of(context).size.width;
    double _screenHeight = MediaQuery.of(context).size.height;

    return Container( color: Colors.white ,child: Image.asset('asset/image/logo.jpg', width: _screenWidth * 0.7));

  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
}

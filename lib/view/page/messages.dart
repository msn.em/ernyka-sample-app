/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'package:flutter/material.dart';


class Messages extends StatefulWidget {

  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    double _screenWidth = MediaQuery.of(context).size.width;
    double _screenHeight = MediaQuery.of(context).size.height;

    return Container(alignment: Alignment.center, child: Icon(Icons.message_sharp, size: 150));
  }
}

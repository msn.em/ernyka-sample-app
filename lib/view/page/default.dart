/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:untitled/common/app_constants.dart';
import 'package:untitled/model/ip_info.dart';
import 'package:untitled/service/app_localizations.dart';
import 'package:untitled/service/rest_api.dart';
import 'package:untitled/state/home_provider.dart';


class Default extends StatefulWidget {

  @override
  _DefaultState createState() => _DefaultState();
}

class _DefaultState extends State<Default> {

  HomeProvider _homeProvider = new HomeProvider();

  void myDataChangeListener(){
    //_homeProvider.notifyListeners();
  }

  @override
  void initState() {
    GetIt.instance.isReady<RestApi>().then((_) => GetIt.instance<RestApi>().addListener(myDataChangeListener));
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) => _homeProvider = Provider.of<HomeProvider>(context, listen: false));
    fetchData();
    super.initState();
  }

  Future<void> fetchData() async{
    await GetIt.instance.allReady();
    IpInfo? ipInfo = await GetIt.instance<RestApi>().getIpAddressInfo();
    if(ipInfo != null){
      _homeProvider.setIpInfoData(ipInfo);
      _homeProvider.setIpInfoStatus(AppConstants.CONNECTION_STATE_DONE);
    } else
      _homeProvider.setIpInfoStatus(AppConstants.CONNECTION_STATE_ERROR);
  }

  @override
  Widget build(BuildContext context) {

    return Consumer<HomeProvider>(builder: (_, __, ___){
      if(_homeProvider.ipInfoDataStatus == AppConstants.CONNECTION_STATE_DONE)
        return Center(child: Directionality(
            textDirection: AppLocalizations.of(context)!.locale.languageCode == 'en' ? TextDirection.rtl : TextDirection.ltr,
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Padding(padding: EdgeInsets.only(bottom: 10),
                  child: Text('${_homeProvider.ipInfo!.ip} ${AppLocalizations.of(context)!.translate('your_ip_address')} ')),
              Text('${_homeProvider.ipInfo!.country} ${AppLocalizations.of(context)!.translate('your_country')} ')
            ])
        ));
      else if(_homeProvider.ipInfoDataStatus == AppConstants.CONNECTION_STATE_WAITING)
        return Center(child: CircularProgressIndicator());
      else
        return Center(child: Text(AppLocalizations.of(context)!.translate('msg_error_connection')));
    });
  }
}

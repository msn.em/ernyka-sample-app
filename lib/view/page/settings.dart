/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled/service/app_language.dart';
import 'package:untitled/service/app_localizations.dart';
import 'package:untitled/state/home_provider.dart';
import 'package:untitled/util/storage_util.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  HomeProvider _homeProvider = new HomeProvider();

  late AppLanguage _appLanguage;
  Locale _faLocale = new Locale('fa', 'IR');
  Locale _enLocale = new Locale('en', 'US');

  Future<Status> _getSettingsStatus() async {
    bool? lang = (await StorageUtil.getDataFromSPInt("KEY_LANG_STATUS") ?? 1) == 1;
    bool? theme = (await StorageUtil.getDataFromSPInt("KEY_THEME_STATUS") ?? 0) == 1;
    return new Status(languageStatus: lang, themeStatus: theme);
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _homeProvider = Provider.of<HomeProvider>(context, listen: false);
    });
  }

  @override
  Widget build(BuildContext context) {
    _appLanguage = Provider.of<AppLanguage>(context);
    double _screenWidth = MediaQuery.of(context).size.width;
    double _screenHeight = MediaQuery.of(context).size.height;

    return Container(
        padding: EdgeInsets.only(left: _screenWidth * 0.25, right: _screenWidth * 0.25),
        child: Center(child: Consumer<HomeProvider>(builder: (_, __, ___) {
          return FutureBuilder<Status>(
              future: _getSettingsStatus(),
              builder: (context, AsyncSnapshot<Status> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting)
                  return Center(child: CircularProgressIndicator());
                else if (snapshot.connectionState == ConnectionState.done && snapshot.hasData)
                  return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      Text(AppLocalizations.of(context)!.translate('theme')),
                      Switch(
                          //value: _themeProvider.isThemeDark,
                          value: snapshot.data!.themeStatus,
                          onChanged: (bool value) {
                            _homeProvider.setTheme(value);
                            int i = value ? 1 : 0;
                            StorageUtil.setDataInSPInt('KEY_THEME_STATUS', i);
                          },
                          activeTrackColor: Colors.blue[900],
                          activeColor: Colors.blueAccent),
                      _homeProvider.isThemeDark
                          ? Text(AppLocalizations.of(context)!.translate('theme_dark'))
                          : Text(AppLocalizations.of(context)!.translate('theme_light'))
                    ]),
                    Container(height: 20),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      Text(AppLocalizations.of(context)!.translate('language')),
                      Switch(
                          //value: _themeProvider.currentLanguage,
                          value: snapshot.data!.languageStatus,
                          onChanged: (bool value) {
                            _homeProvider.setCurrentLanguage(value);
                            int i = value ? 1 : 0;
                            StorageUtil.setDataInSPInt('KEY_LANG_STATUS', i);
                            if (value)
                              _appLanguage.changeLanguage(_faLocale);
                            else
                              _appLanguage.changeLanguage(_enLocale);
                          },
                          activeTrackColor: Colors.blue[900],
                          activeColor: Colors.blueAccent),
                      Text(AppLocalizations.of(context)!.translate('current_lang'))
                    ])
                  ]);
                else
                  return Container();
              });
        })));
  }
}

class Status {
  late bool languageStatus;
  late bool themeStatus;

  Status({required this.languageStatus, required this.themeStatus});
}

/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:untitled/common/configs.dart';
import 'package:untitled/model/ip_info.dart';
import 'package:untitled/state/home_provider.dart';
import 'package:untitled/util/logger.dart';


abstract class RestApi extends ChangeNotifier{

  Future<IpInfo?> getIpAddressInfo();
  Future<String?> sampleWebService();
  Future<String?> anotherWebService();

}



class RestApiImplementation extends RestApi {

  RestApiImplementation(){
    Future.delayed(Duration(milliseconds: 1)).then((_) => GetIt.instance.signalReady(this));
  }

  static const String CLASS_NAME = 'RestApi';

  @override
  Future<IpInfo?> getIpAddressInfo() async {
    try {
      var response = await Dio().request('http://api.myip.com/', options: Options(method: 'GET', sendTimeout: 30, receiveTimeout: 60));
      if (response.statusCode == 200) {
        IpInfo ipInfo = IpInfo.fromJson(jsonDecode(response.data));
        return ipInfo;
      } else{
        Logger.log(CLASS_NAME, "getIpAddressInfo", 'web service error with code: ${response.statusCode}');
        return null;
      }

    } on DioError catch (e) {
      Logger.log(CLASS_NAME, "getIpAddressInfo", e.message);
      return null;
    }
  }

  @override
  Future<String?> sampleWebService() async{
    try {
      var response = await Dio().request('https://jsonplaceholder.typicode.com/todos/1',
          options: Options(method: 'GET', sendTimeout: 30, receiveTimeout: 60));
      if (response.statusCode == 200) {
        return response.data;
      } else{
        Logger.log(CLASS_NAME, "sampleWebService", 'web service error with code: ${response.statusCode}');
        return null;
      }

    } on DioError catch (e) {
      Logger.log(CLASS_NAME, "sampleWebService", e.message);
      return null;
    }
  }

  @override
  Future<String?> anotherWebService() async{
    try {
      var response = await Dio().request('https://jsonplaceholder.typicode.com/photos',
          options: Options(method: 'GET', sendTimeout: 30, receiveTimeout: 60));
      if (response.statusCode == 200) {
        return response.data;
      } else{
        Logger.log(CLASS_NAME, "anotherWebService", 'web service error with code: ${response.statusCode}');
        return null;
      }

    } on DioError catch (e) {
      Logger.log(CLASS_NAME, "anotherWebService", e.message);
      return null;
    }
  }

}

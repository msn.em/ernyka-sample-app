/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AppLanguage extends ChangeNotifier {

  Locale? _appLocale = Locale('fa', 'IR');
  Locale? get appLocal => _appLocale!;

  fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      _appLocale = Locale('fa', 'IR');
      return Null;
    }
    _appLocale = Locale(prefs.getString('language_code')!);
    return Null;
  }


  void changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == Locale('fa', 'IR')) {
      _appLocale = Locale('fa', 'IR');
      await prefs.setString('language_code', 'fa');
      await prefs.setString('countryCode', 'IR');
    } else {
      _appLocale = Locale('en', 'US');
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', 'US');
    }
    notifyListeners();
  }
}

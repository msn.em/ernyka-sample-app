/// Author(s): Mohsen Emami
/// Date Modified: 8/21/2021


class AppConstants{

  static const int CONNECTION_STATE_DONE = 0;
  static const int CONNECTION_STATE_WAITING = 1;
  static const int CONNECTION_STATE_ERROR = 2;
}
/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021



import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:untitled/common/app_constants.dart';
import 'package:untitled/model/ip_info.dart';


class HomeProvider with ChangeNotifier{


  int _currentNavBarIndex = 0;
  int get currentNavBarIndex => _currentNavBarIndex;

  String _currentPageTitle = 'خانه';
  String get currentPageTitle => _currentPageTitle;

  bool _isThemeDark = false;
  bool get isThemeDark => _isThemeDark;

  bool _currentLanguage = true; //true means fa
  bool get currentLanguage => _currentLanguage;

  IpInfo? _ipInfo;
  IpInfo? get ipInfo => _ipInfo;

  int _ipInfoDataStatus = AppConstants.CONNECTION_STATE_WAITING; //default status
  int get ipInfoDataStatus => _ipInfoDataStatus;



  setTheme(bool value){
    _isThemeDark = value;
    notifyListeners();
  }

  setCurrentLanguage(bool value){
    _currentLanguage = value;
    notifyListeners();
  }

  setNavBarIndex(int value){
    _currentNavBarIndex = value;
    notifyListeners();
  }

  setPageTitle(String value){
    _currentPageTitle = value;
    notifyListeners();
  }

  setIpInfoData(IpInfo? value){
    _ipInfo = value;
    notifyListeners();
  }

  setIpInfoStatus(int value){
    _ipInfoDataStatus = value;
    notifyListeners();
  }
}
/// Author(s): Mohsen Emami
/// Date Modified: 8/17/2021


import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:untitled/service/rest_api.dart';
import 'package:untitled/util/storage_util.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:untitled/service/app_language.dart';
import 'package:untitled/service/app_localizations.dart';
import 'package:untitled/view/page/splash.dart';
import 'package:untitled/state/home_provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';


late SharedPreferences preferences;
GetIt getIt = GetIt.instance;


void main() async {
  getIt.registerSingleton<RestApi>(RestApiImplementation(), signalsReady: true);
  WidgetsFlutterBinding.ensureInitialized();
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  preferences = await SharedPreferences.getInstance();
  runApp(Main(appLanguage: appLanguage));
}

class Main extends StatelessWidget {

  final AppLanguage appLanguage;
  Main({required this.appLanguage});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AppLanguage>(create: (_) => appLanguage),
          ChangeNotifierProvider<HomeProvider>(create: (_) => HomeProvider())
        ],
        child: Consumer<AppLanguage>(builder: (context, model, child) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              locale: model.appLocal,
              theme: ThemeData(fontFamily: 'iransans'),
              supportedLocales: [Locale('fa', 'IR'), Locale('en', 'US')],
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate
              ],
              home: Splash());
        }));
  }
}
